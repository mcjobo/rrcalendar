


    calendarApp.controller('CalendarController', ['$scope', 'googleApi', 'googleCalendar', 'googleAuthApi',
function ($scope, googleApi, googleCalendar, googleAuthApi) {

    function ISODateString(d) {
        function pad(n) { return n < 10 ? '0' + n : n }
        return d.getUTCFullYear() + '-'
        + pad(d.getUTCMonth() + 1) + '-'
        + pad(d.getUTCDate()) + 'T'
        + pad(d.getUTCHours()) + ':'
        + pad(d.getUTCMinutes()) + ':'
        + pad(d.getUTCSeconds()) + 'Z'
    };


    $scope.toggleShowCreate = function () {
        if (typeof $scope.showCreateEvent === 'undefined') {
            $scope.showCreateEvent = false;
        }

        if ($scope.showCreateEvent) {
            $scope.showCreateEvent = false;
        } else {
            $scope.showCreateEvent = true;
        }
    };

    $scope.update = function (newEvent) {
        console.log("log", newEvent, ISODateString(newEvent.start));
        var newEventObject = {
            'resource': {
                'end': {
                    'dateTime': ISODateString(newEvent.end)
                },
                'start': {
                    'dateTime': ISODateString(newEvent.start)
                },
                'summary': newEvent.summary,
                'description': createSummary(newEvent),
                "extendedProperties": newEvent.extendedProperties
            }
        };
        console.log("event", newEventObject);
        googleAuthApi.authorize().then(function (auth) {
            console.log("auth", auth);
            googleCalendar.createEvent(newEventObject);
        });
    };

    console.log("googleapi: ", googleApi, googleApi.gapi(), googleApi.gapi().then(function () {
        console.log("success");
    }));
    console.log("calendar called", googleCalendar.makeRequest());
    googleCalendar.getEvents().then(function (data) {
        console.log("events: ", data);
        for(var i = 0; i<data.length; ++i){
            data[i].hiddenDiv = true;
            data[i].start.weekday = formatDate(data[i].start.dateTime);
            data[i].end.weekday = formatDate(data[i].end.dateTime);
        }
        $scope.events = data;
    });

    fillCreate($scope);

    $scope.click = function(event){
        if(event.hiddenDiv){
            event.hiddenDiv = false;
        } else {
            event.hiddenDiv = true;
        }
    };
} ]);

function createSummary(newEvent){
    var summary = "Feuer: " + newEvent.extendedProperties.shared.fire + "   \n";
    summary += "Appell:" + newEvent.extendedProperties.shared.muster + "   \n";
    summary += "Essen:" + newEvent.extendedProperties.shared.food + "  \n";
    summary +=  "Spiel St/Kd:" + newEvent.extendedProperties.shared.gameStKd + "   \n";
    summary +=  "Andacht St/Kd:" + newEvent.extendedProperties.shared.sermonStKd + "   \n";
    summary +=  "Spiel Pf:" + newEvent.extendedProperties.shared.gamePf + "   \n";
    summary +=  "Andacht Pf:" + newEvent.extendedProperties.shared.sermonPf + "   \n";
    return summary;
}

function fillCreate($scope){
    $scope.newEvent = {};
    $scope.newEvent.start = new Date();
    $scope.newEvent.end = new Date();
    $scope.newEvent.summary = 'Stammtreff';
}

function formatDate(date){
    console.log("date", date);
    if(date){
        var weekday = new Array(7);
        weekday[0]=  "Sonntag";
        weekday[1] = "Montag";
        weekday[2] = "Dienstag";
        weekday[3] = "Mittwoch";
        weekday[4] = "Donnerstag";
        weekday[5] = "Freitag";
        weekday[6] = "Samstag";
        var month = new Array(12);
        month[0] = "Januar";
        month[1] = "Februar";
        month[2] = "März";
        month[3] = "April";
        month[4] = "Mai";
        month[5] = "Juni";
        month[6] = "Juli";
        month[7] = "August";
        month[8] = "September";
        month[9] = "Oktober";
        month[10] = "November";
        month[11] = "Dezember";

        return weekday[new Date(date).getDay()];
    }
}