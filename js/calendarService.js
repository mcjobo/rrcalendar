/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var calendarApp = angular.module('calendarApp', []);
  
  // Our auth API integration
  // we should never call this service directly (unless 
  // you really really want to); it's used by the 
  // googleLoginApi service
calendarApp.factory('googleAuthApi',
    ['$window', '$timeout', '$rootScope', '$q', 'googleApi',
    function ($window, $timeout, $rootScope, $q, googleApi) {
        var auth;

        // The authorize function will call google auth.authorize
        // which will authorize this browser client to
        // the user's account along with the appropriate
        // scopes. It will then call `handleAuthResult` with
        // the promise to resolve
        var authorize = function () {
            var d = $q.defer();

            if (auth) {
                d.resolve(auth);
            } else {
                googleApi.gapi().then(function () {
                    var auth = {
                        client_id: '1050344851133-i0lbj73an4srvagahjlahu9b3dst7hiv.apps.googleusercontent.com',
                        scope: 'https://www.googleapis.com/auth/calendar',
                        immediate: false
                    };
                    console.log("before authorize", auth);
                    gapi.auth.authorize(auth, handleAuthResult(d));
                });
            }
            return d.promise;
        };

        // handleAuthResult simple resolves the deferred
        // object if there are no errors. If there are errors
        // then simply reject the promise
        var handleAuthResult = function (defer) {

            return function (authResult) {
                console.log("handleResult", authResult);
                if (authResult && !authResult.error) {
                    auth = authResult;
                    $rootScope.$broadcast('user:authorized', authResult);
                    defer.resolve(authResult);
                } else {
                    defer.reject();
                }
            }
        };

        // return a singleton object
        return {
            authorize: authorize
        }
    } ])
  // Our google Login api service
  calendarApp.factory('googleLoginApi', 
    ['$q', '$rootScope', 'googleAuthApi', 'googleApi',
    function($q, $rootScope, googleAuthApi, googleApi) {
      // Create a load deferred object
      var loadedDefer = $q.defer(),
          loadedPromise = loadedDefer.promise,
          _loggedInUser = null,
          keys = null;
 
      // Create a login function
      // Inside this login function, we'll return a 
      // deferred object and then attempt to authorize
      // and find user data
      var login = function() {
        var d = $q.defer();
 
        // getUserInfo waits until the gapi login 
        // service has loaded (using the loadedPromise)
        // and then immediately calls 
        // gapi.client.oauth2.userinfo.get() to fetch
        // google data. It calls the `success` callback
        // if it's successful and the `fail` callback
        // if unsuccessful
        var getUserInfo = function(success, fail) {
          loadedPromise.then(function() {
            gapi.client.oauth2.userinfo.get()
            .execute(function(resp) {
              if (resp.email) success(resp);
              else fail(resp);
            });
          });
        };
 
        // resolveUserInfo resolves user data
        // from google and takes care of calling the 
        // getUserInfo for us. It will also save and
        // cache the resolved user so we never call the
        // gapi session during the same browser load
        var resolveUserInfo = function(d) {
          getUserInfo(function success(resp) {
            // Resolve the response
            $rootScope.$apply(function() {
              d.resolve(resp);
            });
          },
          // Our failure function
          function fail(resp) {
            // If the response code is 401 (unauthorized)
            // then call authorize on the `googleAuthApi`
            // without being immediate (false)
            // and call resolveUserInfo to get the user's
            // info on load
            if (resp.code === 401) {
              googleAuthApi.authorize(keys, false)
                .then(function() {
                  resolveUserInfo(d);
                });
            } else {
              d.reject(resp);
            }
          });
        };
 
        // Call resolve immediately
        resolveUserInfo(d);
 
        return d.promise;
      }
 
      // call to load the oauth2 module on the 
      // gapi client immediately. 
      // When it's loaded, resolve the loadedDefer object
      googleApi.gapi().then(function(_keys) {
        keys = _keys;
        gapi.client.load('oauth2', 'v2', function () {
          loadedDefer.resolve(keys);
        });
      });
 
      // return our singleton object with two methods
      // login (the login function) and the 
      // getLoggedInStatus which will return a promise
      // that resolves to the user info
      return {
        login: login,
        getLoggedInStatus: function() {
          return loadedPromise;
        }
      }
  }])
  calendarApp.factory('googleCalendar',
    ['$q', 'googleApi',
    function ($q, googleApi) {
        var factory = {};

        var CALENDAR_ID = '3p55kuc87aqo2deo57hdljhsg8@group.calendar.google.com';

        var d = $q.defer();

        factory.listEvents = function () {
            var request = gapi.client.calendar.events.list({
                'calendarId': CALENDAR_ID,
                'orderBy': 'startTime',
                'singleEvents': 'true'
            });
            request.execute(function (response) {
                console.log("response: ", response);
                d.resolve(response.items);
                events = response.items;
            });
        }

        factory.createEvent = function (data) {
            data.calendarId = CALENDAR_ID;
            console.log("data: ", data);
            var request = gapi.client.calendar.events.insert(data);
            console.log("request: ", request);
            request.execute(function (response) {
                console.log("response create: ", response);
                d.resolve(response.items);
                events = response.items;
            });
        }

        factory.makeRequest = function () {
            googleApi.gapi().then(function () {
                console.log("make request", gapi.client.calendar);
                window.gapi.client.load('calendar', 'v3', factory.listEvents);


            });
        }

        factory.getEvents = function () {
            return d.promise;
        }

        return factory;
    } ]);

calendarApp.factory('googleApi',
    ['$window', '$document', '$q', '$rootScope',
    function ($window, $document, $q, $rootScope) {
        // Create a defer to encapsulate the loading of
        // our Google API service. 
        var d = $q.defer();
        var key = 'AIzaSyB_AeT0pAplBrpwhT3HOfsxXD8lQO4JXwE';

        // After the script loads in the browser, we're going
        // to call this function, which in turn will resolve
        // our global defer which enables the 
        $window.bootGoogleApi = function () {
            console.log("keys: ", key);

            // We need to set our API key
            window.gapi.client.setApiKey(key);
            
            $rootScope.$apply(function () {
                d.resolve(key);
            });
        };

        console.log("loading google api");
        // Load client in the browser
        var scriptTag = $document[0].createElement('script');
        scriptTag.type = 'text/javascript';
        scriptTag.async = true;
        scriptTag.src = 'https://apis.google.com/js/client:plusone.js?onload=bootGoogleApi';
        var s = $document[0].getElementsByTagName('body')[0];
        s.appendChild(scriptTag);

        // Return a singleton object that returns the 
        // promise
        return {
            gapi: function () { return d.promise; }
        }
    } ]);